How To Clean A Motorcycle Helmet

Helmets are a very important piece of gear on any motorcycle; however many don't give them adequate care. How often should a helmet be cleaned? Every 3-4 months is good rule of thumb. We have some suggestions for you to follow when cleaning your helmets, and then we will go into detail on how to clean motorcycle helmets below. How

1. How to clean a motorcycle helmet using dishwashing soap.

a.  Dishwashing soap is good for removing bugs that have been baked on, or dried into the paint finish of your helmet. Place some dishwashing soap in a bowl and add warm water until you get a nice soapy mixture, not too wet though.

b.  Take an old toothbrush, or one designated for this purpose only, and begin to scrub the spots on your helmets paint finish gently with the soapy solution. If it has been there long enough it should come off fairly easy; however if its something more stubborn like tar then you may need another approach.

c.  Once you have it all taken off with the dishwashing soap and water, you will want to take some distilled white vinegar diluted in water (50/50), or if you don't care for the smell of vinegar then use something like Meguiars Cleaning Solution.

2. How to clean a motorcycle helmet using Meguiars Cleaner

a.  Take some diluted simple green cleaner, or cheap dollar store window cleaner that is ammonia free, and spray on your helmets paint finish How To Clean A Motorcycle Helmet

b.  Take an old toothbrush again and scrub at the spots on your helmets paint finish gently How To Clean A Motorcycle Helmet.

c.  Once you have it all taken off How To Clean A Motorcycle Helmet, take a clean microfiber cloth and wipe over the entire paint finish How To Clean A Motorcycle Helmet.

d.  Let your helmets paint dry and check for smears, if there are none then you can continue How To Clean A Motorcycle Helmet. If there are smears on the paint then wipe down again but this time with a dry microfiber cloth.


Cleaning your motorcycle helmet usually depends on the kind of shell material it has. If you're not sure, check the owner's manual that came with the helmet.

 A few things to remember: 
Do not use ammonia-based cleaners, like Windex or standard glass cleaner; 
these will eat away at the rubber and plastic in some helmets. Also, avoid using hot water on a motorcycle helmet. 

If you have a lot of grit or grime to remove from your helmet's exterior, use a mild dish soap and warm water mixture instead of ammonia-based products. 

Regularly cleaning the interior of your helmet is just as important as keeping the outside clean. 


For this reason, it is important to regularly wash your helmet liners once a month. 


New source - https://drivrhub.com/
